from django.db import models

from profile.models import Company

class Forum(models.Model):
    pengguna = models.ForeignKey(Company)
    isi = models.CharField('isi', max_length=400, default='DEFAULT VALUE')
    nomor = models.IntegerField(blank=True, null=True)

class Komentar(models.Model):
    forum = models.ForeignKey(Forum, null=True)
    nama = models.CharField('nama', max_length=400, default='DEFAULT VALUE')
    isi = models.CharField('isi', max_length=400, default='DEFAULT VALUE')
