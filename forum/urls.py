from django.conf.urls import url
from .views import index, tambah_forum, tambah_komentar, logout
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^tambah-forum/$', tambah_forum, name='tambah-forum'),
    url(r'^tambah-komentar/$', tambah_komentar, name='tambah-komentar'),
    url(r'^logout/$', logout, name='logout')
]
