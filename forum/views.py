from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
import json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


from .models import Forum, Komentar
from profile.models import Company

# Create your views here.
def index(request):
    if not 'login' in request.session:
        return HttpResponseRedirect(reverse('dashboard:index'))
    else:
        nama = request.session['login']

        pengguna = Company.objects.get(company_name = nama)

        gambar = pengguna.url_foto

        forum = Forum.objects.filter(pengguna = pengguna)

        komentar = kumpulkan_komentar(forum)

        page = request.GET.get('page', 1)
        paginator = Paginator(forum, 3)

        navigasi = paginator.page(page)

        response = {'nama' : nama, 'forum' : forum, 'komentar' : komentar, 'gambar' : gambar, 'navigasi' : navigasi}

        return render(request, 'forum/forum.html', response)

def kumpulkan_komentar(forum):
    komentar = []

    for i in forum:
        a = Komentar.objects.filter(forum = i)
        for j in a:
            komentar.append(j)

    return komentar

@csrf_exempt
def tambah_forum(request):
    if request.method == 'POST':
        nama = request.POST['nama']
        isi = request.POST['isi']
        nomor = request.POST['nomor']

        pengguna = Company.objects.get(company_name = nama)

        forum = Forum()
        forum.pengguna = pengguna
        forum.isi = isi
        forum.nomor = nomor
        forum.save()

        return HttpResponse(status=200)

    else:
        return HttpResponse(status=403)

@csrf_exempt
def tambah_komentar(request):
    if request.method == 'POST':
        isi = request.POST['isi']
        nomor = request.POST['nomor']

        forum = Forum.objects.get(nomor = nomor)

        komentar = Komentar()
        komentar.forum = forum
        komentar.isi = isi
        komentar.nama = request.session['login']
        komentar.save()

        ambil = Komentar.objects.filter(isi = isi)

        return HttpResponse(status=200)

    else:
        return HttpResponse(status=403)

def logout(request):
    request.session.flush() # menghapus semua session

    return HttpResponseRedirect(reverse('dashboard:index'))
