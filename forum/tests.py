from django.test import TestCase, Client
from django.urls import resolve
from .views import index, kumpulkan_komentar, logout
from .models import Forum, Komentar
from profile.models import Company

class tp2Test(TestCase):
    def test_tp2_url_is_exist(self):
        s = self.client.session
        pengguna = Company.objects.create(company_name='contoh')
        s['login'] = 'contoh'
        s.save()
        response = self.client.get('/forum/')
        self.assertEqual(response.status_code, 200)

    def test_belum_login(self):
        response = Client().get('/forum/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response,'/dashboard/',302,200)

    def test_logout(self):
        response = self.client.get('/forum/logout/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response,'/dashboard/',302,200)

    def test_buat_forum(self):
        #Creating a new activity
        pengguna = Company.objects.create(company_name='contoh')
        forum = Forum.objects.create(pengguna=pengguna, isi='tes', nomor=1)
        #Retrieving all available activity
        jumlah = Forum.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_post_buat_forum(self):
        pengguna = Company.objects.create(company_name='contoh')
        response = Client().post('/forum/tambah-forum/', {'nama' : 'contoh', 'isi' : 'contoh', 'nomor' : 1})
        self.assertEqual(response.status_code, 200)

    def test_post_buat_forum_gagal(self):
        response = Client().get('/forum/tambah-forum/', {'nama' : '', 'isi' : '', 'nomor' : 1})
        self.assertEqual(response.status_code, 403)

    def test_buat_komentar(self):
        #Creating a new activity
        pengguna = Company.objects.create(company_name='contoh')
        forum = Forum.objects.create(pengguna=pengguna, isi='tes', nomor=1)
        komentar = Komentar.objects.create(forum=forum, isi='tes', nama='hehe')
        #Retrieving all available activity
        jumlah = Komentar.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_post_buat_komentar(self):
        s = self.client.session
        pengguna = Company.objects.create(company_name='contoh')
        s['login'] = 'contoh'
        s.save()
        pengguna = Company.objects.create(company_name='contoh')
        forum = Forum.objects.create(pengguna=pengguna, isi='tes', nomor=1)
        response = self.client.post('/forum/tambah-komentar/', {'isi' : 'contoh', 'nomor' : 1, 'nama':'hehe'})
        self.assertEqual(response.status_code, 200)

    def test_post_buat_komentar_gagal(self):
        response = Client().get('/forum/tambah-komentar/', {'isi' : '', 'nomor' : 1})
        self.assertEqual(response.status_code, 403)

    def test_tidak_login(self):
        response = Client().get('/forum/')
        self.assertNotIn('login', Client().session.keys())

    def test_kumpul_komentar(self):
        #Creating a new activity
        pengguna = Company.objects.create(company_name='contoh')
        forum = Forum.objects.create(pengguna=pengguna, isi='tes', nomor=1)
        komentar = Komentar.objects.create(forum=forum, isi='tesKomentar')

        forum = Forum.objects.filter(pengguna = pengguna)

        komentar = []

        for i in forum:
            a = Komentar.objects.filter(forum = i)
            for j in a:
                komentar.append(j)

        hasil = kumpulkan_komentar(forum)

        self.assertEqual(komentar, hasil)
