from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse

from django.views.decorators.csrf import csrf_exempt
from .models import Company
from django.core import serializers
import json

# Create your views here.
def index(request):
    # Company.objects.all().delete()
    html ='profile/profile.html'
    return render(request, html)

@csrf_exempt
def createModel(request):
    # Company.objects.all().delete()

    if request.method == 'POST':

        try:
            company = Company.objects.get(company_name=request.POST['name'])
        except:
            company_name = request.POST['name']
            specialty = request.POST['specialty']
            yearFounded = request.POST['year']
            description = request.POST['description']
            website = request.POST['website']
            company_type = request.POST['type']
            url = request.POST['url']
            company = Company(company_name=company_name, specialty=specialty, yearFounded=yearFounded,description=description,website=website,company_type=company_type, url_foto=url)
            company.save()

        data = model_to_dict(company)

        request.session['login'] = request.POST['name']

        print(data)

        return HttpResponse(data)

    else:
        return HttpResponseRedirect(reverse('dashboard:index'))

def model_to_dict(obj):
    data = serializers.serialize('json', [obj, ])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data
