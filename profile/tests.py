from django.test import TestCase
from django.test import Client
from .models import Company

class ProfileUnitTest(TestCase):
    def test_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_model_can_create_new_company(self):
        #Creating a new activity
        company = Company.objects.create(company_name="kelapa", specialty="consumer goods", yearFounded="2017",description="blablabla",website="hehehe",company_type="hahaha", url_foto='hehe')

        #Retrieving all available activity
        counting_all_company= Company.objects.all().count()
        self.assertEqual(counting_all_company,1)

    def test_post_model_can_create_new_company(self):
        #Creating a new activity
        company = Client().post('/profile/createModel/', {'name':"kelapa", 'specialty':"consumer goods", 'year':"2017", 'description':"blablabla", 'website':"hehehe", 'type':"hahaha", 'url':'hehe'})

        #Retrieving all available activity
        counting_all_company= Company.objects.all().count()
        self.assertEqual(counting_all_company,1)

    def test4(self):
        company = Client().get('/profile/createModel/')
        self.assertEqual(company.status_code,302)