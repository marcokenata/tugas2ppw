# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 17:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0003_remove_company_url_foto'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='url_foto',
            field=models.CharField(default='DEFAULT VALUE', max_length=300),
        ),
    ]
