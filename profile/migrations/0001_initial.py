# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-11 18:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('company_name', models.CharField(max_length=50)),
                ('specialty', models.CharField(max_length=250)),
                ('yearFounded', models.CharField(max_length=5)),
                ('description', models.CharField(max_length=300)),
                ('website', models.CharField(max_length=300)),
                ('company_type', models.CharField(max_length=300)),
            ],
        ),
    ]
