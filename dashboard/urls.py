from django.conf.urls import url
from .views import *


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_comment/$', add_comment, name='add_comment'),
]
