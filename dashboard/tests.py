from django.test import TestCase
from django.test import Client
from profile.models import Company
from forum.models import Forum,Komentar
from django.http import HttpResponseRedirect, HttpResponse
from .views import index,kumpulkan_komentar,add_comment

# Create your tests here.
class ReactUnitTest(TestCase):
    def test_dashboard_url_is_exist(self):
        response = Client().get('/dashboard/')
        self.assertEqual(response.status_code, 200)


    def test_kumpul_komentar(self):
        #Creating a new activity
        pengguna = Company.objects.create(company_name='contoh')
        forum = Forum.objects.create(pengguna=pengguna, isi='tes', nomor=1)
        komentar = Komentar.objects.create(forum=forum, isi='tesKomentar')

        forum = Forum.objects.filter(pengguna = pengguna)

        komentar = []

        for i in forum:
            a = Komentar.objects.filter(forum = i)
            for j in a:
                komentar.append(j)

        hasil = kumpulkan_komentar(forum)

        self.assertEqual(komentar, hasil)


    def test_post_buat_komentar(self):
    	pengguna = Company.objects.create(company_name='contoh')
    	forum = Forum.objects.create(pengguna=pengguna, isi='tes', nomor=1)
    	response = self.client.post('/dashboard/add_comment/', {'isi' : 'contoh', 'nomor' : 1, 'nama':'hehe'})
    	self.assertEqual(response.status_code, 200)

    def test_post_buat_komentar_gagal(self):
        response = Client().get('/dashboard/add_comment/', {'isi' : '', 'nomor' : 1})
        self.assertEqual(response.status_code, 403)
