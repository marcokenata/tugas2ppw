from django.shortcuts import render
from forum.models import Forum,Komentar
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# Create your views here.

response = {}
def index(request):
	forum1 = Forum.objects.all()
	komentar = kumpulkan_komentar(forum1)
	html = 'react.html'
	response['gambar'] = 'https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAAjnAAAAJDBhNjYwZGU3LWZkYjMtNDA0NC1iNGQzLWU1YzA4MjE0ZGE2MQ.jpg'
	response['forum'] = forum1
	response['komentar'] = komentar
	page = request.GET.get('page', 1)
	paginator = Paginator(forum1, 3)
	navigasi = paginator.page(page)
	response['navigasi'] = navigasi
	return render(request,html,response)

def kumpulkan_komentar(forum):
    komentar = []

    for i in forum:
        a = Komentar.objects.filter(forum = i)
        for j in a:
            komentar.append(j)

    return komentar

@csrf_exempt
def add_comment(request):
    if request.method == 'POST':
        isi = request.POST['isi']
        nomor = request.POST['nomor']

        forum = Forum.objects.get(nomor = nomor)

        komentar = Komentar()
        komentar.forum = forum
        komentar.isi = isi
        komentar.nama = request.POST['nama']
        komentar.save()

        ambil = Komentar.objects.filter(isi = isi)

        return HttpResponse(status=200)

    else:
        return HttpResponse(status=403)
